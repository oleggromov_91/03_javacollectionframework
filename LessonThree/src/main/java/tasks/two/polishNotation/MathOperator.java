package tasks.two.polishNotation;

import java.util.Objects;

public final class MathOperator {

    private final int priority;
    private final String symbol;

    public MathOperator(int priority, String symbol) {
        this.priority = priority;
        this.symbol = symbol;
    }

    public int getPriority() {
        return priority;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathOperator that = (MathOperator) o;
        return priority == that.priority && symbol.equals(that.symbol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(priority, symbol);
    }
}
