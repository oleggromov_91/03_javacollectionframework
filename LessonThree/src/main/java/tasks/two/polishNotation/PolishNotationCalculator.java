package tasks.two.polishNotation;

import java.util.*;

public class PolishNotationCalculator {

    private final String[] inputMathElements;
    private Stack<String> resultStackMathOperations = new Stack<>();
    private List<String> resultMathElements = new ArrayList<>();
    private final MathOperatorContainer mathOperatorContainer = new MathOperatorContainer();

    private static final String VALID_SYMBOLS = "[\\*,\\/,\\+,\\-,\\(\\)0-9]";

    public PolishNotationCalculator(String inputMathExpression) {
        inputMathElements = preparedInput(inputMathExpression);
    }

    public String calculateResult() {

        for (String mathElement : inputMathElements) {
            try {
                addNumberElementToResult(mathElement);
            } catch (NumberFormatException e) {
                if (resultStackMathOperations.empty()) {
                    resultStackMathOperations.push(mathElement);
                } else {
                    if (elementIsOpenRoundBracket(mathElement) || stackHasLowerPriorityElement(mathElement)) {
                        resultStackMathOperations.push(mathElement);
                    } else {
                        popUntilStackElementHasHigherPriority(mathElement);
                    }
                }
            }
        }
        return preparedFinalResult(resultStackMathOperations, resultMathElements);
    }

    private String[] preparedInput(String inputMathExpression) {
        String[] validInput = Arrays.stream(inputMathExpression.split(""))
                .filter(element -> element.matches(VALID_SYMBOLS))
                .toArray(String[]::new);

        if (inputMathExpression.isBlank() || validInput.length == 0)
            throw new IllegalArgumentException("Input Math Expression is blank, or has no valid symbols! Input expression: \'" + inputMathExpression + "\'");
        else
            return validInput;
    }

    private void addNumberElementToResult(String element) {
        resultMathElements.add(String.valueOf(Integer.parseInt(element)));
    }

    private boolean stackHasLowerPriorityElement(String element) {
        return mathOperatorContainer.getOperator(resultStackMathOperations.peek()).getPriority() < mathOperatorContainer.getOperator(element).getPriority();
    }

    private boolean stackHasSameHigherPriorityElement(String element) {
        return mathOperatorContainer.getOperator(resultStackMathOperations.peek()).getPriority() >= mathOperatorContainer.getOperator(element).getPriority();
    }

    private boolean elementIsOpenRoundBracket(String element) {
        return element.equals(mathOperatorContainer.getOpenRoundBracket());
    }

    private boolean stackTopIsOpenRoundBracket() {
        return mathOperatorContainer.getOperator(resultStackMathOperations.peek()).getSymbol().equals(mathOperatorContainer.getOpenRoundBracket());
    }

    private void popUntilStackElementHasHigherPriority(String element) {

        boolean isWhileRunning = false;

        while (stackHasSameHigherPriorityElement(element)) {
            isWhileRunning = true;
            resultMathElements.add(resultStackMathOperations.pop());
            if (stackTopIsOpenRoundBracket()) {
                resultStackMathOperations.push(element);
                isWhileRunning = false;
                break;
            }
        }
        if (isWhileRunning) {
            resultStackMathOperations.push(element);
        }
    }

    private String preparedFinalResult(Stack<String> resultStackMathOperations, List<String> resultMathElements) {
        String ROUND_BRACKETS = "[(,)]";
        Collections.reverse(resultStackMathOperations);
        resultMathElements.addAll(resultStackMathOperations);
        return String.join("", resultMathElements).replaceAll(ROUND_BRACKETS, "");
    }

}
