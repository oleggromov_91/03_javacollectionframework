package tasks.two.polishNotation;

import java.util.Map;

public class MathOperatorContainer {

    private final Map<String, MathOperator> operators = Map.of(
            "*", new MathOperator(2, "*"),
            "/", new MathOperator(2, "/"),
            "+", new MathOperator(1, "+"),
            "-", new MathOperator(1, "-"),
            ")", new MathOperator(0, ")"),
            "(", new MathOperator(-1, "(")
    );

    private final String openRoundBracket = "(";
    private final String closeRoundBracket = ")";

    public MathOperator getOperator(String operator) {
        return operators.get(operator);
    }

    public String getOpenRoundBracket() {
        return openRoundBracket;
    }

    public String getCloseRoundBracket() {
        return closeRoundBracket;
    }

}
