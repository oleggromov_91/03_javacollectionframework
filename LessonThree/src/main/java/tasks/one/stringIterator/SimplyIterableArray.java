package tasks.one.stringIterator;

import java.util.Iterator;

public class SimplyIterableArray implements Iterator<String> {

    private final String[] values;
    private int index;

    public SimplyIterableArray(String[] values) {
        this.values = values;
    }

    @Override
    public boolean hasNext() {
        return index < values.length;
    }

    @Override
    public String next() {
        return values[index++];
    }

    public void iterate() {
        while (hasNext()) {
            System.out.print(next().concat(", "));
        }
    }


}
