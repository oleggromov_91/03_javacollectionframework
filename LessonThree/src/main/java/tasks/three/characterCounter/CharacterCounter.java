package tasks.three.characterCounter;

import java.util.*;
import java.util.stream.Collectors;

public class CharacterCounter {

    private final Map<String, Integer> result = new TreeMap<>();

    public Map<String, Integer> calculateCharacters(String text) {

        String[] inputText = preparedInput(text);

        for (String character : inputText) {
            character = createReadableSpace(character);
            if (result.containsKey(character)) {
                int value = result.get(character);
                result.put(character, ++value);
            } else {
                result.put(character, 1);
            }
        }
        return sortMapByValue();
    }

    public void flush() {
        result.clear();
    }

    private String[] preparedInput(String inputMathExpression) {
        if (inputMathExpression == null || inputMathExpression.isEmpty()) {
            throw new IllegalArgumentException("Entered string should contain at least one character, or not be null!");
        }
        return inputMathExpression.split("");
    }

    private Map<String, Integer> sortMapByValue() {
        return result.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey,
                        Map.Entry::getValue,
                        (k, v) -> v, LinkedHashMap::new));
    }

    private String createReadableSpace(String element) {
        return (element.matches(" ")) ? "пробел" : element;
    }

}
