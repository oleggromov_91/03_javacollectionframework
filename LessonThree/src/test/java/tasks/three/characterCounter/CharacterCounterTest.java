package tasks.three.characterCounter;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CharacterCounterTest {


    private CharacterCounter characterCounter = new CharacterCounter();


    @Test
    void passWhenExpectedEqualsActual() {
        String input = "aaabbc ccch 1";

        Map<String, Integer> expected = Map.of(
                "c", 4,
                "a", 3,
                "b", 2,
                "пробел", 2,
                "1", 1,
                "h", 1);

        Map<String, Integer> actual = characterCounter.calculateCharacters(input);
        assertEquals(expected, actual);
    }

    @Test()
    void passIfCharacterCounterThrowIllegalArgumentExceptionForNullInput() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new CharacterCounter().calculateCharacters(null));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Entered string should contain at least one character, or not be null!";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test()
    void passIfCharacterCounterThrowIllegalArgumentExceptionForEmptyInput() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new CharacterCounter().calculateCharacters(""));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Entered string should contain at least one character, or not be null!";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void sillyBench() {

        System.out.println("\nSilly bench for character counter:\n Количество символов в тексте : " + LongText.LOREM_IPSUM.length());
        int iterateIndex = 20;

        long startTime = Instant.now().toEpochMilli();
        for (int i = 0; i < iterateIndex; i++) {
            long startOneIterateTime = Instant.now().toEpochMilli();
            characterCounter.calculateCharacters(LongText.LOREM_IPSUM);
            long endOneIterateTime = Instant.now().toEpochMilli();
            System.out.println("Время выполнения одной итерации : " + (endOneIterateTime - startOneIterateTime) / 1000.0 + " seconds");
            characterCounter.flush();
        }
        long endTime = Instant.now().toEpochMilli();
        System.out.println("\nОбщее время character counter  после " + iterateIndex + " итераций: " + (endTime - startTime) / 1000.0 + " seconds");

    }

}