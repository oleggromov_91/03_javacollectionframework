package tasks.one.stringIterator;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

class SimplyIterableArrayTest {

    /**
     * possibly there is nothing to test, but I should demonstrate iterator working :)
     */

    private String[] testArray = Arrays.stream("123490sadj2odh0a9fy1'124ope12jd-asfh23346"
                    .split(""))
            .toArray(String[]::new);


    private final SimplyIterableArray simplyIterableArray = new SimplyIterableArray(testArray);

    @Test
    void iterateIsWork() {
        System.out.println("\nIterator work:\n");
        simplyIterableArray.iterate();
    }

}