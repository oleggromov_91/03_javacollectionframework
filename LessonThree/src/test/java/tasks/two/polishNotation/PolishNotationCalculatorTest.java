package tasks.two.polishNotation;

import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class PolishNotationCalculatorTest {

    @Test
    void passIfResultAssetTestExpression() {
        String expression = "3 + 4 * 2 / (1 - 5)";
        PolishNotationCalculator polishNotationCalculator = new PolishNotationCalculator(expression);

        String expectResult = "342*15-/+";
        String actualResult = polishNotationCalculator.calculateResult();
        assertEquals(expectResult, actualResult);
    }

    @Test
    void passIfExpectedDoNotEqualActual() {
        String expression = "3 + 4 * 2 / (1 - 5)";
        PolishNotationCalculator polishNotationCalculator = new PolishNotationCalculator(expression);

        String expectExpression = "12345*-/+";
        String actualExpression = polishNotationCalculator.calculateResult();
        assertNotEquals(expectExpression, actualExpression);
    }

    @Test()
    void passIfCalculatorThrowNullPointerException() {
        Exception exception = assertThrows(NullPointerException.class, () -> new PolishNotationCalculator(null));
        assertNull(exception.getMessage());
    }

    @Test()
    void passIfCalculatorThrowIllegalArgumentException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> new PolishNotationCalculator("     "));

        String actualMessage = exception.getMessage();
        String expectedMessage = "Input Math Expression is blank, or has no valid symbols!";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void sillyBench() {
        System.out.println("\nSilly bench for polish notation calculator:\n Количество элементов в выражении: " + LongExpression.LOREM_EXPRESSION.length());

        int iterateIndex = 20;

        long startTime = Instant.now().toEpochMilli();
        for (int i = 0; i < iterateIndex; i++) {
            long startOneIterateTime = Instant.now().toEpochMilli();
            PolishNotationCalculator polishNotationCalculator = new PolishNotationCalculator(LongExpression.LOREM_EXPRESSION);
            polishNotationCalculator.calculateResult();
            long endOneIterateTime = Instant.now().toEpochMilli();
            System.out.println("Время выполнения одной итерации : " + (endOneIterateTime - startOneIterateTime) / 1000.0 + " seconds");
        }
        long endTime = Instant.now().toEpochMilli();
        System.out.println("\nОбщее время polish notation calculator после  " + iterateIndex + " итераций: " + (endTime - startTime) / 1000.0 + " seconds");

    }

}